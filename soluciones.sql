﻿USE practica1

SELECT *
    FROM emple;

SELECT *
    FROM depart;

SELECT apellido,oficio 
    FROM emple;

SELECT loc,dept_no
    FROM depart;

SELECT *
    FROM emple 
    ORDER BY apellido ASC;

SELECT *
  FROM emple 
    ORDER BY apellido DESC;

SELECT *
    FROM depart;



SELECT *
    FROM emple 
    ORDER BY dept_no DESC ;

SELECT *
  FROM emple
  ORDER BY dept_no DESC, oficio;

SELECT *
  FROM emple 
    ORDER BY dept_no DESC,apellido ;

SELECT emp_no
  FROM emple 
  WHERE salario>2000;

SELECT emp_no,apellido
  FROM emple 
  WHERE salario<2000;

SELECT *
  FROM emple 
  WHERE salario BETWEEN 1500 AND 2500;
SELECT *
  FROM emple 
  WHERE oficio='Analista';
SELECT *
  FROM emple 
  WHERE oficio='Analista' AND salario>2000;
SELECT apellido,oficio
  FROM emple 
  WHERE dept_no=20;
 SELECT COUNT(*) 
  FROM emple
  WHERE oficio='Vendedor';

SELECT *
  FROM emple
  WHERE apellido like'M%'OR apellido like'N%'
ORDER BY apellido ASC;

/** Ejercicio 22**/

SELECT *
  FROM emple
  WHERE oficio='Vendedor'
  ORDER BY apellido ASC;
 
/** Ejercicio 23**/

 SELECT apellido 
    FROM emple 
    WHERE salario=(
      SELECT MAX(salario) 
        FROM emple
      );

/** Ejercicio 24**/
SELECT *
    FROM emple 
    WHERE dept_no=10 AND oficio='Analista' 
    ORDER BY apellido AND oficio;

/** Ejercicio 25**/

-- opción 1
SELECT DISTINCT MONTH(fecha_alt) 
    FROM emple; 
-- opción 2
SELECT DISTINCT MONTHNAME(fecha_alt)
    FROM emple; 

/** Ejercicio 26**/
SELECT DISTINCT YEAR(fecha_alt)
    FROM emple;
/** Ejercicio 27**/
SELECT DISTINCT DAYOFMONTH(fecha_alt)
  FROM emple;

/** Ejercicio 28**/
SELECT  apellido
  FROM emple
  WHERE salario>2000 OR dept_no=20;

/** Ejercicio 29**/

SELECT apellido,dnombre
    
  FROM depart

/** Ejercicio 30**/
  
  SELECT apellido,oficio,dept_no
  FROM emple;

/** Ejercicio 31**/
  
  SELECT COUNT(*),e.dept_no FROM emple e 
  GROUP BY e.dept_no;

/** Ejercicio 32**/

  SELECT COUNT(*),e.dept_no FROM emple e 
  GROUP BY e.dept_no ;


/** Ejercicio 33**/

  SELECT apellido FROM emple
  ORDER BY oficio AND emp_no;

/** Ejercicio 34**/
 
 SELECT apellido FROM emple
  WHERE apellido LIKE 'A%';
  
/** Ejercicio 35**/
 
 SELECT apellido FROM emple
  WHERE apellido LIKE 'A%' OR apellido LIKE 'M%';
  
/** Ejercicio 36**/
 
 SELECT apellido FROM emple 
  WHERE apellido NOT LIKE '%Z';

/** Ejercicio 37 **/
 
 SELECT DISTINCT apellido FROM emple 
  WHERE apellido LIKE 'A%' AND oficio LIKE '%E%'
  ORDER BY oficio,salario DESC; 







  


  



 


    